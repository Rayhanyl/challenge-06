const carsService = require("../../../services/carsService");

const create = async (req, res, next) => {
  const { name, price, size, available } = req.body;

  const { status, status_code, message, data } = await carsService.create({
    name,
    price,
    size,
    available,
    createdBy: req.user.email,
  });

  res.status(status_code).send({
    status: status,
    message: message,
    data: data,
  });
};

const getAll = async (req, res) => {
  const { status, status_code, message, data } = await carsService.getAll();

  res.status(status_code).send({
    status: status,
    message: message,
    data: data,
  });
};

const deleteCars = async (req, res, next) => {
  const { id } = req.params;

  const deletedBy = req.user.name;
  const { status, status_code, message, data } = await carsService.deleteCars({
    id,
    deletedBy: req.user.email,
  });

  res.status(status_code).send({
    status: status,
    message: message,
    data: data,
  });
};

const updateCars = async (req, res, next) => {
  const { id } = req.params;
  const { name, price, size, available } = req.body;

  const { status, status_code, message, data } = await carsService.updateCars({
    id,
    name,
    price,
    size,
    available,
    updatedBy: req.user.email,
  });

  res.status(status_code).send({
    status: status,
    message: message,
    data: data,
  });
};

const getByAvailable = async (req, res) => {
  const { available } = req.body;

  const { status, code_status, message, data } =
    await carsService.getByAvailable({ available });

  res.status(code_status).send({
    status: status,
    message: message,
    data: data,
  });
};
module.exports = { create, getAll, deleteCars, updateCars, getByAvailable };
