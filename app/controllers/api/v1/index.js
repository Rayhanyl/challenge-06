/**
 * @file contains entry point of controllers api v1 module
 * @author Fikri Rahmat Nurhidayat
 */

const carsController = require("./carsController");
const usersController = require("./usersController");
const authController = require("./authController");

module.exports = {
  carsController,
  usersController,
  authController,
};
