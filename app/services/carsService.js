const carsRepository = require("../repositories/carsRepository");

class CarsService {
  static async create({ name, price, size, available, createdBy }) {
    try {
      if (!name) {
        return {
          status: false,
          status_code: 400,
          message: "Name wajib diisi",
          data: {
            created_cars: null,
          },
        };
      }

      if (!price) {
        return {
          status: false,
          status_code: 400,
          message: "Price wajib diisi",
          data: {
            created_cars: null,
          },
        };
      }

      if (!size) {
        return {
          status: false,
          status_code: 400,
          message: "Size wajib diisi",
          data: {
            created_cars: null,
          },
        };
      }
      if (!available) {
        return {
          status: false,
          status_code: 400,
          message: "Available wajib diisi",
          data: {
            created_cars: null,
          },
        };
      }

      const createdCar = await carsRepository.create({
        name,
        price,
        size,
        available,
        createdBy,
      });

      return {
        status: true,
        status_code: 201,
        message: "Car created successfully",
        data: {
          created_car: createdCar,
        },
      };
    } catch (err) {
      return {
        status: false,
        status_code: 500,
        message: err.message,
        data: {
          registered_user: null,
        },
      };
    }
  }

  static async getAll() {
    try {
      const getCars = await carsRepository.getAll();

      return {
        status: true,
        status_code: 201,
        message: "Successfully",
        data: {
          data_cars: getCars,
        },
      };
    } catch (err) {
      return {
        status: false,
        code_status: 500,
        message: err.message,
        data: {
          data_cars: null,
        },
      };
    }
  }

  static async deleteCars({ id, deletedBy }) {
    try {
      const deletedCar = await carsRepository.deleteCars({ id, deletedBy });

      return {
        status: true,
        status_code: 200,
        message: "Car deleted successfully",
        data: {
          deleted_car: deletedCar,
        },
      };
    } catch (err) {
      return {
        status: false,
        status_code: 500,
        message: err.message,
        data: {
          deleted_car: null,
        },
      };
    }
  }

  static async updateCars({ id, name, price, size, available, updatedBy }) {
    try {
      if (!name) {
        return {
          status: false,
          status_code: 400,
          message: "Name wajib diisi",
          data: {
            created_cars: null,
          },
        };
      }

      if (!price) {
        return {
          status: false,
          status_code: 400,
          message: "Price wajib diisi",
          data: {
            created_cars: null,
          },
        };
      }

      if (!size) {
        return {
          status: false,
          status_code: 400,
          message: "Size wajib diisi",
          data: {
            created_cars: null,
          },
        };
      }
      if (!available) {
        return {
          status: false,
          status_code: 400,
          message: "Available wajib diisi",
          data: {
            created_cars: null,
          },
        };
      }

      const updatedCar = await carsRepository.updateCars({
        id,
        name,
        price,
        size,
        available,
        updatedBy,
      });

      return {
        status: true,
        status_code: 201,
        message: "Car updated successfully",
        data: {
          updated_car: updatedCar,
        },
      };
    } catch (err) {
      return {
        status: false,
        status_code: 500,
        message: err.message,
        data: {
          updated_car: null,
        },
      };
    }
  }

  static async getByAvailable({ available }) {
    try {
      const getCars = await carsRepository.getAll();

      const filtered = getCars.filter((Cars) => {
        if (Cars.available == true) {
          return Cars;
        }
      });

      return {
        status: true,
        code_status: 201,
        message: "data cars berhasil ditampilkan",
        data: {
          data_cars: filtered,
        },
      };
    } catch (err) {
      return {
        status: false,
        code_status: 500,
        message: err.message,
        data: {
          data_cars: null,
        },
      };
    }
  }
}

module.exports = CarsService;
